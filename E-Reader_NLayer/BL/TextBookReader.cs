﻿using System;
using EReader.BL.Domain;

namespace EReader.BL
{
    public class TextBookReader : IReader
    {
        private TextBook _textBook = null;
        private int _currentPage = 0;
        private ushort _pageSize = 35;

        public TextBookReader(TextBook bookToRead)
        {
            _textBook = bookToRead;
        }
        
        public object Previous()
        {
            if (_currentPage > 1) _currentPage--;
            else _currentPage = 1;

            return GetPage(_currentPage);
        }
        
        public object Next()
        {
            double numberOfPages = Math.Ceiling((double)_textBook.Content.Length / _pageSize);
            if ((double)_currentPage < numberOfPages) _currentPage++;
            else _currentPage = 1;

            return GetPage(_currentPage);
        }
        
        private string GetPage(int pageNumber)
        {
            int remainingTextSize = _textBook.Content.Length - (_pageSize * (pageNumber - 1));
            if (remainingTextSize < 0) remainingTextSize = 0;

            return _textBook.Content.Substring((pageNumber - 1) * _pageSize, Math.Min(_pageSize, remainingTextSize));
        }
    }
}