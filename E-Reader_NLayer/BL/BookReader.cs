﻿using System;
using EReader.BL.Domain;

namespace EReader.BL
{
    public class BookReader : IReader
    {
        private IReader _specificReader;

        public BookReader(Book bookToRead)
        {
            this.LoadBook(bookToRead);
        }

        public object Previous()
        {
            return _specificReader.Previous();
        }

        public object Next()
        {
            return _specificReader.Next();
        }

        private void LoadBook(Book bookToRead)
        {
            // C# 7: switch with pattern matching
            switch (bookToRead)
            {
                case TextBook textBook:
                    _specificReader = new TextBookReader(textBook);
                    break;
                case ImageBook imageBook:
                    _specificReader = new ImageBookReader(imageBook);
                    break;
                default:
                    throw new InvalidCastException("Unable to load the book!");
            }
        }
    }
}