﻿namespace EReader.BL
{
    public interface IReader
    {
        object Previous();
        object Next();
    }
}