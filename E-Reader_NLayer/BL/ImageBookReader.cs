﻿using System.Linq;
using EReader.BL.Domain;

namespace EReader.BL
{
    public class ImageBookReader : IReader
    {
        private ImageBook _imageBook = null;
        private int _currentPosition = 0;

        public ImageBookReader(ImageBook bookToRead)
        {
            _imageBook = bookToRead;
        }
        
        public object Previous()
        {
            if (_currentPosition > 1 && _currentPosition <= _imageBook.Images.Count())
                _currentPosition--;
            else
                _currentPosition = 1;

            return GetImage(_currentPosition);
        }

        public object Next()
        {
            if (_currentPosition > 0 && _currentPosition < _imageBook.Images.Count())
                _currentPosition++;
            else
                _currentPosition = 1;

            return GetImage(_currentPosition);
        }
        
        private char GetImage(int imagePosition)
        {
            return _imageBook.Images.ElementAt(imagePosition - 1);
        }
    }
}