﻿using EReader.BL.Domain;

namespace EReader.UI.CA.Extensions
{
    public static class ImageBookExtensions
    {
        public static string Summary(this Book book)
        {
            return book.Title;
        }
    }
}