﻿using System;
using EReader.BL;
using EReader.BL.Domain;
using EReader.UI.CA.Extensions;

namespace EReader.UI.CA
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.Run();
        }

        void Run()
        {
            // TextBook
            string title = "C# Language";
            string author = "KdG";
            string text = "C# is one of the programming languages designed"
                          + " for the Common Language Infrastructure";
            TextBook tb = new TextBook(Isbn.GetNewIsbn(), "C# Language", "KdG", text);
            Console.WriteLine("Reading book: {0}", tb.ToString());
            IReader tbr = new BookReader(tb);
            Console.WriteLine("Next: " + tbr.Next());
            Console.WriteLine("Next: " + tbr.Next());
            Console.WriteLine("Next: " + tbr.Next());
            Console.WriteLine("Prev: " + tbr.Previous());
            Console.WriteLine("Next: " + tbr.Next());
            Console.WriteLine("Next: " + tbr.Next());
            Console.WriteLine("Next: " + tbr.Next());

            Console.WriteLine();

            // ImageBook
            char[] images = { 'A', 'B', 'C', 'D' };
            ImageBook ib = new ImageBook(Isbn.GetNewIsbn(), "Some Images", "KdG", images);
            Console.WriteLine(ib.Summary());
            Console.WriteLine("Reading book: {0}", ib.ToString());
            IReader ibr = new BookReader(ib);
            Console.WriteLine("Next: " + ibr.Next());
            Console.WriteLine("Next: " + ibr.Next());
            Console.WriteLine("Next: " + ibr.Next());
            Console.WriteLine("Prev: " + ibr.Previous());
            Console.WriteLine("Next: " + ibr.Next());
            Console.WriteLine("Next: " + ibr.Next());
            Console.WriteLine("Next: " + ibr.Next());

            Console.ReadLine(); // keep console open! (wait for <ENTER>)
        }
    }
}