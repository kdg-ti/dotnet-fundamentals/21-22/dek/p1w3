﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EReader.BL.Domain
{
    public class ImageBook : Book
    {
        public IEnumerable<char> Images { get; private set; }

        public ImageBook(Isbn isbn, string title, string author, IEnumerable<char> images)
            : base(isbn, title, author, BookFormat.CBR)
        {
            Images = images;
        }

        public override string GetInfo() // 'new' <> 'override'?  |  'sealed'?
        {
            return String.Format("ImageBook: '{0}', by {1} ({2} pics)", 
                Title, 
                Author, 
                Images.Count<char>());
        }
    }
}