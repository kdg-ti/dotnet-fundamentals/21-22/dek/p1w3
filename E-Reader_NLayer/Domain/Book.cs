﻿namespace EReader.BL.Domain
{
    public abstract class Book
    {
        private readonly Isbn _isbn;
        
        public Isbn Isbn { 
            get { return _isbn; } 
        }
        public string Title { get; private set; }
        public string Author { get; private set; }
        public BookFormat Format { get; }
        
        public Book(Isbn isbn, string title, string author, BookFormat format = BookFormat.TXT)
        {
            this._isbn = isbn;
            this.Title = title;
            this.Author = author;
            this.Format = format;
        }

        public abstract string GetInfo();
        
        public sealed override string ToString()
        {
            return $"Book: '{Title}', by {Author} ({Isbn}, {Format})";
        }
    }
}