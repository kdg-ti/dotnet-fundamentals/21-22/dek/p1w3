﻿using System;
using System.Text.RegularExpressions;

namespace EReader.BL.Domain
{
    public sealed class Isbn
    {
        private readonly int _year;
        private readonly int _number;

        private Isbn(int year, int number)
        {
            this._year = year;
            this._number = number;
        }

        public static Isbn Parse(string isbnString)
        {
            if (Regex.IsMatch(isbnString, @"^\d{4}-\d{4}$"))
            {
                int year = Int32.Parse(isbnString.Substring(0, 4));
                int number = Int32.Parse(isbnString.Substring(5, 4));
                return new Isbn(year, number);
            }
            else
                throw new FormatException("Invalid ISBN-format!");
        }
        
        public static bool TryParse(string isbnString, out Isbn isbn)
        {
            bool isMatch = Regex.IsMatch(isbnString, @"^\d{4}-\d{4}$");

            if (isMatch)
            {
                int year;
                bool isYearOk = Int32.TryParse(isbnString.Substring(0, 4), out year);
                int number;
                bool isNumberOk = Int32.TryParse(isbnString.Substring(5, 4), out number);
                isbn = new Isbn(year, number);
            }
            else
                isbn = null; // NOT: throw new FormatException("Invalid ISBN-format!");

            return isMatch;
        }
        
        public override string ToString() // 'override'<>'new'
        {
            return String.Format("{0:d4}-{1:d4}", _year, _number);
        }
        
        #region Generate new isbn-number
        private static int _prevYear;
        private static int _prevNumber;
    
        public static Isbn GetNewIsbn()
        {
            if (Isbn._prevYear != DateTime.Today.Year)
            {
                Isbn._prevYear = DateTime.Today.Year;
                Isbn._prevNumber = 0;
            }
            return new Isbn(Isbn._prevYear, ++Isbn._prevNumber);
        }
        #endregion
    }
}