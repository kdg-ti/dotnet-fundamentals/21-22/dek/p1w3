﻿namespace EReader.BL.Domain
{
    public enum BookFormat
    {
        EPUB = 0,
        PDF,
        TXT,
        CBR // 'Comic Book Archive' (collection of images compressed as RAR)
    }
}