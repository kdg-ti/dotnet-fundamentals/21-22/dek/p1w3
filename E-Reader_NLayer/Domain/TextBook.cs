﻿using System;

namespace EReader.BL.Domain
{
    public class TextBook : Book
    {
        public string Content { get; private set; }

        public TextBook(Isbn isbn, string title, string author, string content)
            : base(isbn, title, author, BookFormat.TXT)
        {
            Content = content;
        }

        public override string GetInfo()
        {
            return String.Format("TextBook: '{0}', by {1} ({2} chars)", 
                Title, 
                Author, 
                Content.Length);
        }
    }
}